package org.example.phonebook.ui.menu.item;

import lombok.RequiredArgsConstructor;
import org.example.phonebook.entities.User;
import org.example.phonebook.exception.DuplicateContactException;
import org.example.phonebook.exception.InvalidInputException;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.ui.menu.MenuItem;
import org.example.phonebook.ui.view.AuthView;

@RequiredArgsConstructor
public class SignInMenuItem implements MenuItem {
    private final AuthService authService;
    private final AuthView contactView;

    @Override
    public String getName() {
        return "Войти";
    }

    @Override
    public void execute() {
        User user = null;
        try {
            user = contactView.readUser();
        } catch (InvalidInputException e) {
            System.out.println("Invalid data");
        } catch (DuplicateContactException exception) {
            System.out.println("Duplicate data");

        }
        authService.login(user);
    }

    @Override
    public boolean isAuth(){
        return true;
    }
}
