package org.example.phonebook.ui.menu;

import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Scanner;
@AllArgsConstructor
public class Menu {
    Scanner scanner;
    List<MenuItem> items;

    public void run(){
        for (;;){
            showMenu();
            int choice=getUserChoice();
            if (choice < 0 || choice>= items.size()){
                System.out.println("Incorrect choice");
                continue;
            }
            items.get(choice).execute();
            if (items.get(choice).isFinal()) break;
        }
    }

    private int getUserChoice() {
        System.out.println("Enter your choice");
        int ch=scanner.nextInt();
        scanner.nextLine();
        return ch-1;
    }

    private void showMenu(){
        System.out.println("---------------------------------------");
        for (int i = 0; i < items.size(); i++) {
            if (!items.get(i).isAuth()){
                continue;
            }
            items.get(i).getName();
            System.out.printf("%2d - %s\n",i+1, items.get(i).getName());
        }
        System.out.println("---------------------------------------");
    }
}
