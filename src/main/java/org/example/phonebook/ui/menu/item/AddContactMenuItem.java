package org.example.phonebook.ui.menu.item;

import lombok.RequiredArgsConstructor;
import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;
import org.example.phonebook.exception.AuthException;
import org.example.phonebook.exception.DuplicateContactException;
import org.example.phonebook.exception.InvalidInputException;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.ui.menu.MenuItem;
import org.example.phonebook.services.ContactService;
import org.example.phonebook.ui.view.ContactView;

@RequiredArgsConstructor
public class AddContactMenuItem implements MenuItem {
    private final ContactService contactService;
    private final AuthService authService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Добавить контакт";
    }

    @Override
    public void execute() {
        Contact contact = null;
        if (!isAuth()){
            throw new AuthException();
        }
        try {
            contact = contactView.readContact();
        } catch (InvalidInputException e) {
            System.out.println("Invalid data");
        } catch (DuplicateContactException exception) {
            System.out.println("Duplicate data");

        }
        contactService.save(contact);
    }

    @Override
    public boolean isAuth(){
        return authService.isAuth();
    }
}
