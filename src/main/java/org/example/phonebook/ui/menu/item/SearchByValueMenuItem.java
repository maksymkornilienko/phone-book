package org.example.phonebook.ui.menu.item;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.example.phonebook.exception.AuthException;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.ui.menu.MenuItem;
import org.example.phonebook.services.ContactService;
import org.example.phonebook.ui.view.ContactView;

@RequiredArgsConstructor
public class SearchByValueMenuItem implements MenuItem {
    private final ContactService contactService;
    private final AuthService authService;
    private final ContactView contactView;
    @Override
    public String getName() {
        return "Поиск по началу контакта";
    }

    @Override
    public void execute() {
        if (!isAuth()){
            throw new AuthException();
        }
        contactView.showContact(contactService.findByValueStart(contactView.readValue()));
        contactView.pause();
    }

    @Override
    public boolean isAuth(){
        return authService.isAuth();
    }
}
