package org.example.phonebook.ui.menu.item;

import lombok.RequiredArgsConstructor;
import org.example.phonebook.exception.AuthException;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.ui.menu.MenuItem;
import org.example.phonebook.services.ContactService;
import org.example.phonebook.ui.view.ContactView;


@RequiredArgsConstructor
public class DeleteContactMenuItem implements MenuItem {
    private final ContactService contactService;
    private final AuthService authService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Удалить контакт";
    }

    @Override
    public void execute() {
        if (!isAuth()){
            throw new AuthException();
        }
        contactService.deleteByValue(contactView.readValue());
    }

    @Override
    public boolean isAuth(){
        return authService.isAuth();
    }
}
