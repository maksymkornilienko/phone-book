package org.example.phonebook.ui.menu.item;

import org.example.phonebook.ui.menu.MenuItem;

public class ExitMenuItem implements MenuItem {
    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public void execute() {
        System.out.println("Good bye!!!");
    }

    @Override
    public boolean isFinal() {
        return true;
    }

    @Override
    public boolean isAuth(){
        return true;
    }
}
