package org.example.phonebook.ui.menu.item;

import lombok.AllArgsConstructor;
import org.example.phonebook.exception.AuthException;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.ui.menu.MenuItem;
import org.example.phonebook.services.ContactService;
import org.example.phonebook.ui.view.ContactView;

@AllArgsConstructor
public class ShowEmailMenuItem implements MenuItem {
    private final ContactService contactService;
    private final AuthService authService;
    private final ContactView contactView;
    @Override
    public String getName() {
        return "Посмотреть только email";
    }

    @Override
    public void execute() {
        if (!isAuth()){
            throw new AuthException();
        }
        contactView.showContact(contactService.findByEmail());
        contactView.pause();
    }

    @Override
    public boolean isAuth(){
        return authService.isAuth();
    }
}
