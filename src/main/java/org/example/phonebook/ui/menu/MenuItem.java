package org.example.phonebook.ui.menu;

public interface MenuItem {
    String getName();
    void execute();
    default boolean isFinal(){
        return false;
    }
    boolean isAuth();

}
