package org.example.phonebook.ui.menu.item;

import lombok.RequiredArgsConstructor;
import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.User;
import org.example.phonebook.exception.DuplicateContactException;
import org.example.phonebook.exception.InvalidInputException;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.services.ContactService;
import org.example.phonebook.ui.menu.MenuItem;
import org.example.phonebook.ui.view.AuthView;
import org.example.phonebook.ui.view.ContactView;

@RequiredArgsConstructor
public class RegisterMenuItem implements MenuItem {
    private final AuthService authService;
    private final AuthView authView;

    @Override
    public String getName() {
        return "Зарегистироваться";
    }

    @Override
    public void execute() {
        User user = null;
        try {
            user = authView.readUser();
        } catch (InvalidInputException e) {
            System.out.println("Invalid data");
        }

        authService.register(user);
    }

    @Override
    public boolean isAuth(){
        return true;
    }
}
