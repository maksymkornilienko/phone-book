package org.example.phonebook.ui.view;

import lombok.RequiredArgsConstructor;
import org.example.phonebook.entities.User;

import java.util.Scanner;

@RequiredArgsConstructor
public class SignInView implements AuthView{
    private final Scanner scanner;


    @Override
    public User readUser() {
        User user = new User();

        System.out.println("Enter user login: ");
        String login = scanner.nextLine();
        user.setLogin(login);

        System.out.println("Enter user password: ");
        String pass = scanner.nextLine();
        user.setPassword(pass);

        return user;
    }
}
