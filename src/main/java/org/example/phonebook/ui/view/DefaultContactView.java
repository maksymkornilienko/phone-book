package org.example.phonebook.ui.view;

import lombok.RequiredArgsConstructor;
import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;
import org.example.phonebook.exception.InvalidInputException;

import java.util.List;
import java.util.Scanner;

@RequiredArgsConstructor
public class DefaultContactView implements ContactView{
    private final Scanner scanner;

    @Override
    public Contact readContact() {
        String name=readName();
        System.out.println("Enter contact surname: ");
        String surname = scanner.nextLine();
        ContactType contactType = readContactType();
        String value=readValue();
        return new Contact().setName(name).setSurname(surname).setContactType(contactType).setValue(value);
    }

    @Override
    public String readValue() {
        System.out.println("Enter contact value: ");
        return scanner.nextLine();
    }
    @Override
    public String readName() {
        System.out.println("Enter contact name: ");
        return scanner.nextLine();
    }

    private ContactType readContactType() {
        ContactType[] types = ContactType.values();
        for (int i = 0; i < types.length; i++) {
            System.out.printf("%d - %s\n", i+1, types[i].getName());
        }
        int choice = getChoice();
        if (choice<0 || choice>=types.length){
            throw new InvalidInputException();
        }
        return types[choice];
    }

    private int getChoice() {
        System.out.println("Enter contact type: ");
        int choice = scanner.nextInt()-1;
        scanner.nextLine();
        return choice;
    }

    @Override
    public void showContact(List<Contact> contactList) {
        System.out.println("-".repeat(115));
        System.out.printf("|%40s|%13s|%27s|%30s|\n","Id","Type","Name","Value");
        System.out.println("-".repeat(115));
        for (Contact contact : contactList) {
            System.out.printf("|%40s|%13s|%27s|%30s|\n",
                    contact.getId(),
                    contact.getContactType().getName(),
                    contact.getFullName(),
                    contact.getValue()
            );
        }
        System.out.println("-".repeat(115));
    }

    @Override
    public void pause() {
        scanner.nextLine();
    }
}
