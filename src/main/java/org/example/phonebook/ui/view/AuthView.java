package org.example.phonebook.ui.view;

import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.User;
import org.example.phonebook.services.AuthService;

import java.util.List;

public interface AuthView {
    User readUser();
}
