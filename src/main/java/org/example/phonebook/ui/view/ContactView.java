package org.example.phonebook.ui.view;

import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;

import java.util.List;

public interface ContactView {
    Contact readContact();
    String readValue();
    String readName();
    void showContact(List<Contact> contactList);
    void pause();
}
