package org.example.phonebook.util;

import org.example.phonebook.persistance.ContactRepository;
import org.example.phonebook.persistance.NetworkContactRepository;
import org.example.phonebook.services.NetworkAuthService;

public class NetworkContactRepositoryFactory implements ContactRepositoryFactory{
    private NetworkAuthService networkAuthService;

    @Override
    public ContactRepository createContactRepository() {
        return new NetworkContactRepository(networkAuthService);
    }
    @Override
    public NetworkAuthService createAuthService() {
        if (networkAuthService == null){
            networkAuthService=new NetworkAuthService();
        }
        System.out.println(networkAuthService);
        return networkAuthService;
    }
}
