package org.example.phonebook.util;

import org.example.phonebook.persistance.ContactRepository;
import org.example.phonebook.services.AuthService;

public interface ContactRepositoryFactory {
    ContactRepository createContactRepository();
    AuthService createAuthService();
}
