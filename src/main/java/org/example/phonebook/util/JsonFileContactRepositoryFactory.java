package org.example.phonebook.util;

import org.example.phonebook.persistance.ContactRepository;
import org.example.phonebook.persistance.InJsonContactRepository;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.services.InMemoryAuthService;

public class JsonFileContactRepositoryFactory implements ContactRepositoryFactory{
    @Override
    public ContactRepository createContactRepository() {
        return new InJsonContactRepository();
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }
}
