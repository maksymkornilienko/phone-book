package org.example.phonebook.util.validator.contact;

import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;
import org.example.phonebook.util.validator.Validator;

import java.util.regex.Pattern;

public class EmailValidator implements Validator<Contact> {
    public static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @Override
    public boolean isValid(Contact contact) {
        if (contact.getContactType()!= ContactType.EMAIL) return true;
        return EMAIL_PATTERN.matcher(contact.getValue()).matches();
    }
}
