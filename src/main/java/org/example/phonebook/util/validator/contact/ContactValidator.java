package org.example.phonebook.util.validator.contact;

import lombok.RequiredArgsConstructor;
import org.example.phonebook.entities.Contact;
import org.example.phonebook.util.validator.Validator;

import java.util.List;
@RequiredArgsConstructor
public class ContactValidator implements Validator<Contact> {
    private final List<Validator<Contact>> validators;

    @Override
    public boolean isValid(Contact contact) {
        return validators.stream().allMatch(v->v.isValid(contact));
    }
}
