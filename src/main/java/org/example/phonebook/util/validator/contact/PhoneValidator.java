package org.example.phonebook.util.validator.contact;

import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;
import org.example.phonebook.util.validator.Validator;

import java.util.regex.Pattern;

public class PhoneValidator implements Validator<Contact> {
    public static final Pattern PHONE_PATTERN = Pattern.compile("(\\+380|80|0)\\d{9}");
    @Override
    public boolean isValid(Contact contact) {
        if (contact.getContactType()!= ContactType.PHONE) return true;

        return PHONE_PATTERN.matcher(contact.getValue()).matches();
    }
}
