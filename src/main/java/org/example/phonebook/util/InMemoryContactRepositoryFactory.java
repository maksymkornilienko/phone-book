package org.example.phonebook.util;

import org.example.phonebook.persistance.ContactRepository;
import org.example.phonebook.persistance.InMemoryContactRepository;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.services.InMemoryAuthService;

public class InMemoryContactRepositoryFactory implements ContactRepositoryFactory{
    @Override
    public ContactRepository createContactRepository() {
        return new InMemoryContactRepository();
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }
}
