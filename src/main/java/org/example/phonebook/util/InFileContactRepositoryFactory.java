package org.example.phonebook.util;

import org.example.phonebook.persistance.ContactRepository;
import org.example.phonebook.persistance.InFileContactRepository;
import org.example.phonebook.persistance.InMemoryContactRepository;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.services.InMemoryAuthService;

public class InFileContactRepositoryFactory implements ContactRepositoryFactory {
    @Override
    public ContactRepository createContactRepository() {
        return new InFileContactRepository();
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }
}
