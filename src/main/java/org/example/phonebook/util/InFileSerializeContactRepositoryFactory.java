package org.example.phonebook.util;

import org.example.phonebook.persistance.ContactRepository;
import org.example.phonebook.persistance.InFileSerializeContactRepository;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.services.InMemoryAuthService;

public class InFileSerializeContactRepositoryFactory implements ContactRepositoryFactory{
    @Override
    public ContactRepository createContactRepository() {
        return new InFileSerializeContactRepository();
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }
}
