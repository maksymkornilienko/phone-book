package org.example.phonebook;

import org.example.phonebook.entities.Contact;
import org.example.phonebook.services.AuthService;
import org.example.phonebook.ui.menu.Menu;
import org.example.phonebook.services.ContactService;
import org.example.phonebook.ui.menu.item.*;
import org.example.phonebook.ui.view.ContactView;
import org.example.phonebook.ui.view.DefaultContactView;
import org.example.phonebook.ui.view.RegisterView;
import org.example.phonebook.ui.view.SignInView;
import org.example.phonebook.util.ContactRepositoryFactory;
import org.example.phonebook.util.NetworkContactRepositoryFactory;
import org.example.phonebook.util.validator.contact.ContactValidator;
import org.example.phonebook.util.validator.contact.EmailValidator;
import org.example.phonebook.util.validator.contact.PhoneValidator;
import org.example.phonebook.util.validator.Validator;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ContactRepositoryFactory contactRepositoryFactory = new NetworkContactRepositoryFactory();
        Validator<Contact> contactValidator = new ContactValidator(List.of(new PhoneValidator(), new EmailValidator()));
        AuthService authService = contactRepositoryFactory.createAuthService();
        ContactService contactService = new ContactService(contactRepositoryFactory.createContactRepository(), contactValidator);
        Scanner scanner = new Scanner(System.in);
        ContactView contactView = new DefaultContactView(scanner);
        Menu menu = new Menu(scanner, List.of(
                new AddContactMenuItem(contactService,authService,contactView),
                new ShowContactMenuItem(contactService,authService,contactView),
                new ShowPhoneMenuItem(contactService,authService,contactView),
                new ShowEmailMenuItem(contactService,authService,contactView),
                new SearchByNameMenuItem(contactService,authService,contactView),
                new SearchByValueMenuItem(contactService,authService,contactView),
                new DeleteContactMenuItem(contactService,authService,contactView),
                new SignInMenuItem(authService,new SignInView(scanner)),
                new RegisterMenuItem(authService,new RegisterView(scanner)),
                new ExitMenuItem()
        ));
        menu.run();
    }
}
