package org.example.phonebook.services;

import org.example.phonebook.entities.User;

public interface AuthService {
    void login(User user);
    void register(User user);
    boolean isAuth();
}
