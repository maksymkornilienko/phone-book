package org.example.phonebook.services;


import lombok.RequiredArgsConstructor;
import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;
import org.example.phonebook.exception.ContactNotFoundException;
import org.example.phonebook.exception.DuplicateContactException;
import org.example.phonebook.exception.ValidateException;
import org.example.phonebook.persistance.ContactRepository;
import org.example.phonebook.util.validator.Validator;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class ContactService {
    private final ContactRepository contactRepository;
    private final Validator<Contact> contactValidator;

    public void save(Contact contact) {
        if (!contactValidator.isValid(contact)){
            throw new ValidateException("asdasdasd");
        }
        if (contactRepository.findByValue(contact.getValue()).isPresent()){
            throw new DuplicateContactException();
        }
        contactRepository.save(contact);
    }

    public List<Contact> findAll() {
        return contactRepository.findAll();
    }

    public List<Contact> findByEmail() {
        return contactRepository.findByType(ContactType.EMAIL);
    }

    public List<Contact> findByPhone() {
        return contactRepository.findByType(ContactType.PHONE);
    }

    public List<Contact> findByValueStart(String valueStart) {
        return contactRepository.findByValueStart(valueStart);
    }

    public Optional<Contact> findByValue(String value) {
        return contactRepository.findByValue(value);
    }

    public List<Contact> findByName(String namePart) {
        return contactRepository.findByName(namePart);
    }

    public void deleteByValue(String value) {
        contactRepository.findByValue(value).orElseThrow(ContactNotFoundException::new);
        contactRepository.deleteByValue(value);
    }

}
