package org.example.phonebook.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import org.example.phonebook.entities.User;
import org.example.phonebook.entities.response.UserResponse;
import org.example.phonebook.exception.AuthException;
import java.net.http.HttpResponse;

public class NetworkAuthService implements AuthService {
    @Getter
    private String token;

    @Override
    public void login(User user) {
        ObjectMapper objectMapper = new ObjectMapper();
        String payload = null;
        try {
            payload = objectMapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        HttpService httpService = new HttpService();
        HttpResponse<String> response = httpService.auth("login", payload);
        if (response.statusCode()==200) {
            parseToken(response.body());
        }
    }

    @Override
    public void register(User user) {
        ObjectMapper objectMapper = new ObjectMapper();
        String payload = null;
        try {
            payload = objectMapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        HttpService httpService = new HttpService();
        HttpResponse<String> response = httpService.auth("register", payload);
        if (response.statusCode() != 200) {
            throw new AuthException();
        }
        login(user);
    }

    @Override
    public boolean isAuth() {
        return token!=null;
    }
    private void parseToken(String responseBody){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            token = objectMapper.readValue(responseBody, UserResponse.class).getToken();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
