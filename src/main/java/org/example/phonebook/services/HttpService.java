package org.example.phonebook.services;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpService {
    public HttpResponse<String> getContacts(String url,String token){
        HttpRequest request = getRequest(url)
                .setHeader("Authorization", "Bearer "+token)
                .build();

        return makeResponse(request);
    }

    public HttpResponse<String> saveContact(String url, String token, String payload) {
        HttpRequest request = postRequest(url, payload)
                .setHeader("Authorization", "Bearer "+token)
                .build();

        return makeResponse(request);
    }

    public HttpResponse<String> auth(String url, String payload) {
        HttpRequest request = postRequest(url, payload)
                .build();

        return makeResponse(request);
    }
    private HttpRequest.Builder getRequest(String url){
        return makeRequest(url)
                .GET();
    }

    private HttpRequest.Builder postRequest(String url,String payload){
        return makeRequest(url)
                .POST(HttpRequest.BodyPublishers.ofString(payload));
    }

    private HttpRequest.Builder makeRequest(String url){
        return HttpRequest.newBuilder()
                .GET()
                .setHeader("Accept", "application/json")
                .setHeader("Content-Type", "application/json")
                .uri(URI.create("https://mag-contacts-api.herokuapp.com/"+url));
    }

    private HttpResponse<String> makeResponse(HttpRequest request){
        HttpClient client = HttpClient.newHttpClient();
        HttpResponse<String> response =
                null;
        try {
            response = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }
}
