package org.example.phonebook.services;

import org.example.phonebook.entities.User;
import org.example.phonebook.exception.AuthException;

public class InMemoryAuthService implements AuthService {
    public static final String LOGIN = "LOGIN";
    public static final String PASS = "PASS";
    private boolean auth = false;

    @Override
    public void login(User user) {
        if (!user.getLogin().equals(LOGIN) || !user.getPassword().equals(PASS)){
            throw new AuthException();
        }
        auth = true;
    }

    @Override
    public void register(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isAuth() {
        return auth;
    }
}
