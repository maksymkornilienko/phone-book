package org.example.phonebook.entities.contact;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ContactType {
    EMAIL("email"),PHONE("phone");
    @Getter
    @JsonValue
    private final String name;
}
