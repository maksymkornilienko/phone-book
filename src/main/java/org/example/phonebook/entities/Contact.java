package org.example.phonebook.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.Accessors;
import org.example.phonebook.entities.contact.ContactType;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;
import java.util.UUID;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties({"surname"})
public class Contact implements Serializable {
    private String id;
    private String name;
    private String surname;
    private ContactType contactType;
    private String value;
    public static Comparator<Contact> bySurnameComparator(){
        return (Contact a, Contact b)->{
            if (a==null&&b==null) return 0;
            if (a==null) return 1;
            if (b==null) return -1;

            int cmp = Objects.compare(a.surname,b.surname,String::compareTo);
            if (cmp==0){
                cmp=Objects.compare(a.name,b.name,String::compareTo);
            }
            return cmp;
        };

    }

    public String getFullName() {
        return surname+" "+name;
    }
}
