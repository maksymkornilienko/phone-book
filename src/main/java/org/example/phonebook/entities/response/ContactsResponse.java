package org.example.phonebook.entities.response;

import lombok.Data;
import lombok.experimental.Accessors;
import org.example.phonebook.entities.Contact;

import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
public class ContactsResponse implements Serializable {
    private List<Contact> contacts;
    private String status;
}
