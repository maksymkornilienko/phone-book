package org.example.phonebook.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.example.phonebook.entities.contact.ContactType;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

@Data
@Accessors(chain = true)
public class User implements Serializable {
    private String login;
    private String password;
    @JsonProperty("date_born")
    private String dateBorn;
}
