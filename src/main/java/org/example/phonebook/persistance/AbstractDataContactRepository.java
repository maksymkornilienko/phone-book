package org.example.phonebook.persistance;

import org.apache.commons.lang3.ArrayUtils;
import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class AbstractDataContactRepository implements ContactRepository {
    @Override
    public void save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        List<Contact> contactList=getData();
        contactList.add(contact);
        setData(contactList);
    }

    @Override
    public List<Contact> findAll() {
        return getData().stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByType(ContactType contactType) {
        return getData().stream()
                .filter(contact -> contact.getContactType()==contactType)
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return getData().stream()
                .filter(contact -> contact.getValue().startsWith(valueStart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return getData().stream()
                .filter(contact -> contact.getValue().equals(value))
                .findFirst();
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return getData().stream()
                .filter(contact -> (contact.getFullName()).contains(namePart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public void deleteByValue(String value) {
        List<Contact> contactsList=getData().stream()
                .filter(contact -> !contact.getValue().equals(value))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toCollection(ArrayList::new));
        setData(contactsList);
    }

    protected abstract void setData(List<Contact> contactsList);

    protected abstract List<Contact> getData();

    protected void createFile(String fileName){
        File file = new File(fileName);
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected int getContactTypeIndex(ContactType contactType){
        return ArrayUtils.indexOf(ContactType.values(),contactType);
    }
}
