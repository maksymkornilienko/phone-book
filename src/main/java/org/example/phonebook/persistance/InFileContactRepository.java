package org.example.phonebook.persistance;

import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class InFileContactRepository extends AbstractDataContactRepository {
    private final static String FILENAME = "contacts.txt";

    @Override
    protected void setData(List<Contact> contactsList){
        String contactsContent = contactsList.stream()
                .map(p->String.format("%s:%s:%s:%s:%s",p.getId(),p.getSurname(),p.getName(),getContactTypeIndex(p.getContactType()),p.getValue())).collect(Collectors.joining("\n"));
        try(OutputStream outputStream = new FileOutputStream(FILENAME)) {
            outputStream.write(contactsContent.getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    protected List<Contact> getData() {
        List<Contact> contactsList = new ArrayList<>();
        createFile(FILENAME);
        try(InputStream inputStream = new FileInputStream(FILENAME)) {
            Scanner sc = new Scanner(inputStream);
            while (sc.hasNextLine()){
                String line = sc.nextLine();
                if (line.trim().isEmpty()) continue;
                String[] parts = line.split(":");
                int indexType = Integer.parseInt(parts[3]);
                contactsList.add(new Contact().setId(parts[0])
                        .setSurname(parts[1])
                        .setName(parts[2])
                        .setContactType(ContactType.values()[indexType])
                        .setValue(parts[4]));
            }
        } catch (EOFException ex){
            return new ArrayList<>();
        }catch (IOException e){
            e.printStackTrace();
            }
        return contactsList;
    }
}
