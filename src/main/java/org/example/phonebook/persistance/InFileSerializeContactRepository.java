package org.example.phonebook.persistance;

import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class InFileSerializeContactRepository extends AbstractDataContactRepository {
    private final static String FILENAME="contacts.dat";

    @Override
    protected void setData(List<Contact> contactsList){
        try(ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(FILENAME))) {
            outputStream.writeInt(contactsList.size());
            for (Contact c : contactsList){
                outputStream.writeObject(c);
            }
            outputStream.flush();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    protected List<Contact> getData() {
        List<Contact> contactsList = new ArrayList<>();
        createFile(FILENAME);
        try(ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(FILENAME))) {
            int size = inputStream.readInt();
            contactsList = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                Contact c = (Contact) inputStream.readObject();
                contactsList.add(c);
            }
        } catch (EOFException ex){
            return new ArrayList<>();
        }catch (IOException|ClassNotFoundException e){
            e.printStackTrace();
        }
        return contactsList;
    }
}
