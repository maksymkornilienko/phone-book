package org.example.phonebook.persistance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.nio.file.Files.readString;

public class InJsonContactRepository extends AbstractDataContactRepository {
    private final static String FILENAME="contacts.json";

    @Override
    protected void setData(List<Contact> contactsList){
        Gson gson = new GsonBuilder().serializeNulls().create();
        String json = gson.toJson(contactsList);
        Path path =Path.of("contacts.json");
        try {
            Files.writeString(path, json, StandardOpenOption.WRITE,StandardOpenOption.TRUNCATE_EXISTING,StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected List<Contact> getData() {
        List<Contact> contactsList = new ArrayList<>();
        createFile(FILENAME);
        Gson gson = new GsonBuilder().serializeNulls().create();
        try {
            contactsList = gson.fromJson(readString(Path.of(FILENAME)), new TypeToken<ArrayList<Contact>>(){}.getType());
        }catch (IOException e) {
            e.printStackTrace();
        }
        if (contactsList==null){
            return new ArrayList<>();
        }
        return contactsList;
    }
}
