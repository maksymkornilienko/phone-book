package org.example.phonebook.persistance;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.response.ContactsResponse;
import org.example.phonebook.services.HttpService;
import org.example.phonebook.services.NetworkAuthService;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class NetworkContactRepository extends AbstractDataContactRepository {
    private final NetworkAuthService networkAuthService;
    private List<Contact> contactsList=new ArrayList<>();

    @Override
    public void save(Contact contact) {
        ObjectMapper objectMapper = new ObjectMapper();
        String payload = null;
        try {
            payload = objectMapper.writeValueAsString(contact);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        HttpService httpService = new HttpService();
        HttpResponse<String> response = httpService.saveContact(
                "contacts/add",
                networkAuthService.getToken(),
                payload
        );
        if (response.statusCode()==200){
            contactsList = new ArrayList<>();
        }else {
            throw new RuntimeException();
        }
    }

    @Override
    protected void setData(List<Contact> contactsList){

    }

    @Override
    protected List<Contact> getData() {
        if (contactsList.size()>0){
            return contactsList;
        }
        ObjectMapper objectMapper = new ObjectMapper();
        HttpService httpService = new HttpService();

        HttpResponse<String> response = httpService.getContacts("contacts", networkAuthService.getToken());

        try {
            contactsList = objectMapper.readValue(response.body(), ContactsResponse.class).getContacts();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return contactsList;
    }
}
