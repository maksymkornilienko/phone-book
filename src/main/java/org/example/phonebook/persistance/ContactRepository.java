package org.example.phonebook.persistance;


import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;

import java.util.List;
import java.util.Optional;

public interface ContactRepository {
    void save(Contact contact);
    List<Contact> findAll();
    List<Contact> findByType(ContactType contactType);
    List<Contact> findByValueStart(String valueStart);
    Optional<Contact> findByValue(String value);
    List<Contact> findByName(String namePart);
    void deleteByValue(String value);
}
