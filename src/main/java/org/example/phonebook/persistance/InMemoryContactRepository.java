package org.example.phonebook.persistance;

import org.example.phonebook.entities.Contact;
import org.example.phonebook.entities.contact.ContactType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class InMemoryContactRepository implements ContactRepository {
    private List<Contact> contactsList=new ArrayList<>();

    @Override
    public void save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        contactsList.add(contact);
    }

    @Override
    public List<Contact> findAll() {
        return contactsList.stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByType(ContactType contactType) {
        return contactsList.stream()
                .filter(contact -> contact.getContactType()==contactType)
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return contactsList.stream()
                .filter(contact -> contact.getValue().startsWith(valueStart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return contactsList.stream()
                .filter(contact -> contact.getValue().equals(value))
                .findFirst();
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return contactsList.stream()
                .filter(contact -> (contact.getFullName()).contains(namePart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public void deleteByValue(String value) {
        contactsList=contactsList.stream()
                .filter(contact -> !contact.getValue().equals(value))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
